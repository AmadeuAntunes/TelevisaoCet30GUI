﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TelevisaoCet30GUI
{
    public partial class Televisão : Form
    {

        private Tv MinhaTV;
        public Televisão()
        {
            InitializeComponent();
            MinhaTV = new Tv();
            LabelVolume.Text = MinhaTV.GetVolume().ToString();


            LabelStatus.Text = MinhaTV.EnviaMensagem();
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (MinhaTV.GetEstado())
            {

                MinhaTV.DesligaTv();
                ButtonOnOff.Text = "On";
                ButtonDiminuiCanal.Enabled = false;
                ButtonAumentaCanal.Enabled = false;
                trackBarVolume.Enabled = false;
                LabelStatus.ForeColor = Color.Red;
                LabelStatus.Text = MinhaTV.EnviaMensagem();
                LabelCanal.Text = " -- ";
                ecra.ImageLocation = @"..\..\img\desligado.jpg";

            }
            else
            {
                MinhaTV.LigaTv();
                ButtonOnOff.Text = "Off";
                ButtonDiminuiCanal.Enabled = true;
                ButtonAumentaCanal.Enabled = true;
                trackBarVolume.Enabled = true;
                LabelStatus.Text = MinhaTV.EnviaMensagem();
                LabelStatus.ForeColor = Color.Green;
                LabelCanal.Text = MinhaTV.GetCanal().ToString();
                trackBarVolume.Value = MinhaTV.GetVolume();
                mudaimagem(MinhaTV.GetCanal());
            }

        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            MinhaTV.MudaCanal(MinhaTV.GetCanal() + 1);
            LabelCanal.Text =MinhaTV.GetCanal().ToString();
            mudaimagem(MinhaTV.GetCanal());
        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {
            if(MinhaTV.GetCanal() - 1 > -1)
            {
                MinhaTV.MudaCanal(MinhaTV.GetCanal() - 1);
                LabelCanal.Text = MinhaTV.GetCanal().ToString();
                mudaimagem(MinhaTV.GetCanal());
            }
           
        }
        private void mudaimagem(int canal)
        {
            ecra.ImageLocation = @"..\..\img\desligado.jpg";
            switch (canal)
            {
                case 1: ecra.ImageLocation = @"..\..\img\1.jpg"; break;
                default:
                    break;
            }
        } 

        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            MinhaTV.SetVolume(trackBarVolume.Value);
            LabelVolume.Text = trackBarVolume.Value.ToString();
        }
    }
}
